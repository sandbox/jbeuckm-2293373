
(function ($) {

    Drupal.behaviors.titanium_connector = {

        attach: function(context, settings) {

            /**
             * Send a message to Titanium code.
             * @param type
             * @param object
             */
            window.tellTi = function(type, object) {

                var obj = {
                    type: type,
                    object: object
                };

                parent.postMessage(obj, '*');

            };

            function messageReceive(evt) {
                tellWebview(evt.data.type, evt.data.object);
            }
            window.addEventListener('message', messageReceive, false);



            /**
             * Here Titanium sends a message to javascript running in the webview.
             *
             * @param type
             * @param object
             */
            window.tellWebview = function(type, object) {
                switch (type) {

                    case "FILL_INPUT":
                        alert(object.name+" => "+object.value);
                        jQuery("input[name="+object.name+"]").val(object.value);
                        break;

                    case "NAVIGATE":
                        window.location.href = Drupal.settings.basePath + object.url;
                        break;
                }
            };


            tellTi("DOM_READY");
        }
    };

})(jQuery);

